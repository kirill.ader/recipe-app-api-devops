terraform {
  backend "s3" {
    bucket         = "temp-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

# add my ownkey to EC2 keypairs
# resource "aws_key_pair" "kirill_ader" {
#   key_name   = "kirill_ader"
#   public_key = file("./my_key.pub")
# }
